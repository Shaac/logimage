module Bitmap (bitmap, bitmapSolved) where

--
-- Exported functions.
--

-- | Get the bitmap of the nonogram from the clues.
bitmap :: Int -> [[Int]] -> [[Int]] -> [[Bool]]
bitmap d l c = edge d l c $ grid (length l) (length c)

-- | Get the bitmap of the solved nonogram from the clues and the image bitmap.
bitmapSolved :: Int -> [[Int]] -> [[Int]] -> [[Bool]] -> [[Bool]]
bitmapSolved d l c g = edge d l c $ solve $ grid (length l) (length c)
  where
    solve = zipWith (zipWith (||)) $ map (>>= replicate 10) g >>= replicate 10

--
-- Internal functions.
--

-- | Get a nonogram bitmap, from the clues and the centers’s bitmap.
edge :: Int -> [[Int]] -> [[Int]] -> [[Bool]] -> [[Bool]]
edge d l c = link (corner d l c) (topClues c) (leftClues l)

-- | Link 4 bitmaps into one.
link :: [[a]] -> [[a]] -> [[a]] -> [[a]] -> [[a]]
link topL topR botL botR = zipWith (++) (topL ++ botL) (topR ++ botR)

-- | Get the bitmap of the top left corner.
corner :: Int -> [[Int]] -> [[Int]] -> [[Bool]]
corner d l c = replicate (size c - 20) (blanks w) ++ level ++ credits
  where
    size    = (10 *) . maximum . (2 :) . (map length) -- Minimal size of 20 px.
    w       = size l
    credits = line $ if w >= 50 then zipWith (++) softBy shaac else shaac
    line x  = zipWith (++) (repeat $ blanks $ w - (length $ head x)) x
    level   = line $ zipWith (++) lvl $ replicate 3 (blanks 8) ++ (number d) ++ (replicate 2 (blanks 8))
    blanks  = flip replicate False

-- | Get the bitmap of the top clues.
topClues :: [[Int]] -> [[Bool]]
topClues clues = joinColumns $ drawLines $ equalize $ intsToBitmaps clues
  where
    intsToBitmaps = map (>>= ((blanks 3 ++) . (++ blanks 2) . number))
    equalize      = map (\c -> blanks (size - (length c)) ++ c)
    drawLines     = zipWith (map . (++) . begin) [0 :: Int ..]
    joinColumns   = foldl (zipWith (++)) (repeat [])
    blanks        = flip replicate $ replicate 8 False
    size          = maximum (2 : (map length clues)) * 10
    begin column  = [True, column `mod` 5 == 0]

-- | Get the bitmap of the left clues.
leftClues :: [[Int]] -> [[Bool]]
leftClues clues = (>>= aux) $ zip [0 :: Int ..] $ intsToBitmaps clues
  where
    intsToBitmaps = map $ foldr (zipWith ((++) . (++ [False, False])) . number) (replicate 5 [])
    size    = maximum (2 : (map length clues)) * 10
    aux (i,l) = wrap i $ map (replicate (size - (length $ head l)) False ++) l
    wrap i l = [black, replicate size (i `mod` 5 == 0), white] ++ l ++ [white, white]
    black = replicate size True
    white = replicate size False


-- | Get the pixel grid of the nonogram grid (without the clues).
grid :: Int -> Int -> [[Bool]]
grid height weight = map (line (10 * weight)) [0 .. 10 * height -1]
  where
    line size i = if isBlack i then replicate size True else fill size
    fill size   = map isBlack [0 .. size - 1]
    isBlack i   = i `mod` 10 == 0 || i `mod` 50 == 1

number :: Int -> [[Bool]]
number n
  | n < 10 = map (\x -> [False, False] ++ x ++ (replicate 3 False)) (font n)
  | n < 100 = zipWith (\x -> \y -> x ++ (False : y) ++ [False])
    (font (n `div` 10)) (font (n `mod` 10))
  | otherwise = error "Too big."

font :: Int -> [[Bool]]
font 0 = [[True, True, True], [True, False, True], [True, False, True],
    [True, False, True], [True, True, True]]
font 1 = [[True, True, False], [False, True, False], [False, True, False],
    [False, True, False], [True, True, True]]
font 2 = [[True, True, True], [False, False, True], [True, True, True],
    [True, False, False], [True, True, True]]
font 3 = [[True, True, True], [False, False, True], [False, True, True],
    [False, False, True], [True, True, True]]
font 4 = [[True, False, True], [True, False, True], [True, True, True],
    [False, False, True], [False, False, True]]
font 5 = [[True, True, True], [True, False, False], [True, True, True],
    [False, False, True], [True, True, True]]
font 6 = [[True, True, True], [True, False, False], [True, True, True],
    [True, False, True], [True, True, True]]
font 7 = [[True, True, True], [True, False, True], [False, False, True],
    [False, False, True], [False, False, True]]
font 8 = [[True, True, True], [True, False, True], [True, True, True],
    [True, False, True], [True, True, True]]
font 9 = [[True, True, True], [True, False, True], [True, True, True],
    [False, False, True], [True, True, True]]
font _ = error "No font."

shaac :: [[Bool]]
shaac = replicate 3 (replicate 20 False) ++
    [[False, True, True, False, True, False, True, False, True, True,
    True, False, True, True, True, False, True, True, True, False],
    [True, False, False, False, True, False, True, False, True, False, True,
    False, True, False, True, False, True, False, True, False],
    [True, True, True, False, True, True, True, False, True, True, True, False,
    True, True, True, False, True, False, False, False],
    [False, False, True, False, True, False, True, False, True, False, True,
    False, True, False, True, False, True, False, True, False],
    [True, True, False, False, True, False, True, False, True, False, True,
    False, True, False, True, False, True, True, True, False]]
    ++ replicate 2 (replicate 20 False)

softBy :: [[Bool]]
softBy = replicate 3 (replicate 30 False) ++
    [[False, True, True, False, False, True, False, False, True, True, True,
    False, True, True, True, False, False, False, False, True, True, False,
    False, True, False, True, False, False, False, False],
    [True, False, False, False, True, False, True, False, True, False, False,
    False, False, True, False, False, False, False, False, True, False, True,
    False, True, False, True, False, False, False, False],
    [True, True, True, False, True, False, True, False, True, True, True,
    False, False, True, False, False, False, False, False, True, True, False,
    False, False, True, False, False, False, False, False],
    [False, False, True, False, True, False, True, False, True, False, False,
    False, False, True, False, False, False, False, False, True, False, True,
    False, False, True, False, False, False, False, False],
    [True, True, False, False, False, True, False, False, True, False, False,
    False, False, True, False, False, False, False, False, True, True, False,
    False, False, True, False, False, False, False, False]]
    ++ replicate 2 (replicate 30 False)

lvl :: [[Bool]]
lvl = replicate 3 (replicate 12 False) ++
    [[True, False, False, False, True, False, True, False, True, False, False,
    False],
    [True, False, False, False, True, False, True, False, True, False, False,
    False],
    [True, False, False, False, True, False, True, False, True, False, False,
    False],
    [True, False, False, False, True, False, True, False, True, False, False,
    False],
    [True, True, True, False, False, True, False, False, True, True, True,
    False]]
    ++ replicate 2 (replicate 12 False)
