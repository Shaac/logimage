import Nonogram (puzzle, solveD', done, boolGrid)
import Png (writePng)
import Bitmap (bitmap, bitmapSolved)

-- Given an ASCII nonogram image, get the nonogram clues.
clues :: [[Char]] -> ([[Int]], [[Int]])
clues ascii = (map parseLine ascii, map parseLine (columns ascii))
    where columns = foldr (zipWith (:)) (repeat [])

-- Given a line of an ASCII nonogram line, get the line clues.
parseLine :: [Char] -> [Int]
parseLine = filter (/= 0) . (aux 0)
    where
        aux n []       = [n]            -- end of line
        aux n ('-':xs) = n:(aux 0 xs)   -- end any existing block
        aux n ('X':xs) = aux (n + 1) xs -- continue current block
        aux _ _        = error "Invalid entry"

-- Get clues from ASCII in standard input.
readClues :: IO ([[Int]], [[Int]])
readClues = do
    n <- getLine
    ascii <- sequence $ replicate (read n) getLine
    return $ clues $ map (map head . words) ascii

main :: IO ()
main = do
    name <- getLine
    (c, l) <- readClues
    let (steps, p) = solveD' $ puzzle c l
    if done p then do
        let d = steps `div` 10 + 1
        writePng (name ++ ".png") $ bitmap d c l
        writePng (name ++ "_solved.png") $ bitmapSolved d c l (boolGrid p)
    else
        return ()
