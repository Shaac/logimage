#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PIL import Image

im = Image.open(sys.argv[1])
a,b,x,y = im.getbbox()
print(sys.argv[2])
print(y)
for i in range(y):
    txt = ''
    for j in range(x):
        a = im.getpixel((j,i))
        if a < 50:
            txt += 'X '
        else:
            txt += '- '
    print(txt[:-1])
