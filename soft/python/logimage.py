#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import Image
import ImageDraw
import ImageFont
import time

"""
On lit l'image en entrée.
En première ligne on a le nom du logimage
En deuxième ligne on a la dimension x du logimage de taille x*y.
Ensuite on a l'image de la forme suivante :
X X -
- X -
X X X
"""
temps = time.clock()
class logimage():

    def fromtext(self):

        n = int(raw_input())
        self.frommatrix([raw_input().rsplit() for i in xrange(n)])

    def frommatrix(self, matrix):

        self.n = len(matrix)
        self.m = len(matrix[0])
        self.lignes = [[0 for i in xrange(self.m)] for j in xrange(self.n)]
        self.colonnes = [[0 for i in xrange(self.n)] for j in xrange(self.m)]
        self.l_finies = [False for i in xrange(self.n)]
        self.c_finies = [False for i in xrange(self.m)]
        for i, ligne in enumerate(matrix):
            longueur = 0
            numero = 1
            for j in ligne:
                if j != '-' and j != 'X':
                    exit('Erreur d\'entrée')
                elif j == '-' and longueur != 0:
                    self.lignes[i][numero] = longueur
                    self.lignes[i][0] = numero
                    numero += 1
                    longueur = 0
                elif j == 'X':
                    longueur += 1
            if longueur != 0:
                self.lignes[i][numero] = longueur
                self.lignes[i][0] = numero
        for i in xrange(self.m):
            longueur = 0
            numero = 1
            for j in xrange(self.n):
                if matrix[j][i] == '-' and longueur != 0:
                    self.colonnes[i][numero] = longueur
                    self.colonnes[i][0] = numero
                    numero += 1
                    longueur = 0
                elif matrix[j][i] == 'X':
                    longueur += 1
            if longueur != 0:
                self.colonnes[i][numero] = longueur
                self.colonnes[i][0] = numero
        self.compteur = 0

    def rempli(self, modele, test, c, i, rectangle, espaces):
        r = 0
        # On teste avec un blanc :
        if espaces > 0 and  modele[i] != 1:
            if self.rempli(modele, test, c, i + 1, rectangle, espaces - 1) > 0:
                test[i][1] += 1
                r = 1

        # Ou avec un noir :
        if 0 in [modele[j] for j in xrange(i, i + c[rectangle])]:
            return(r)

        n = len(modele)
        if i + c[rectangle] < n:
            # On se préoccupe du rectangle suivant :
            if modele[i + c[rectangle]] == 1:
                return(r)
            if rectangle < c[0]:
                if 0== self.rempli(modele, test, c, i + c[rectangle] + 1,
                    rectangle + 1, espaces):
                    return(r)
            else:
                for j in xrange(i + c[rectangle]+1, n):
                    if modele[j] == 1:
                        return(r)
                for j in xrange(i + c[rectangle]+1, n):
                    test[j][1] += 1
            test[i+c[rectangle]][1] += 1
        for j in xrange(c[rectangle]):
            test[i + j][0] += 1
        return(r+1)

    def solve(self,m,indetermines=-1):
        if indetermines == -1:
            indetermines = self.n * self.m
        bak = 0
        while (indetermines != 0):
            if bak == indetermines:
                break
            bak = indetermines
            self.compteur += 1
            for i in xrange(self.m):
                if self.c_finies[i]:
                    continue
                test = [[0,0] for j in xrange(self.n)]
                self.rempli([m[j][i] for j in xrange (self.n)], test, self.colonnes[i], 0, 1,
                    min(self.n - sum(self.colonnes[i]) + 1, self.n))
                for j, [noir, blanc] in enumerate(test):
                    if (noir == 0) and (blanc != 0):
                        if m[j][i] == -1: indetermines -= 1
                        m[j][i] = 0
                    if (noir != 0) and (blanc == 0):
                        if m[j][i] == -1: indetermines -= 1
                        m[j][i] = 1
                if not -1 in [m[j][i] for j in xrange(self.n)]:
                    self.c_finies[i] = True
            for i in xrange(self.n):
                if self.l_finies[i]:
                    continue
                test = [[0,0] for j in xrange(self.m)]
                self.rempli(m[i], test, self.lignes[i], 0, 1, min(self.m - sum(self.lignes[i]) + 1,self.m))
                for j, [noir, blanc] in enumerate(test):
                    if (noir == 0) and (blanc != 0):
                        if m[i][j] == -1: indetermines -= 1
                        m[i][j] = 0
                    if (noir != 0) and (blanc == 0):
                        if m[i][j] == -1: indetermines -= 1
                        m[i][j] = 1
                if not -1 in m[i]:
                    self.l_finies[i] = True
        return(m, indetermines)

    def first_unknown(self, m):
        for (i, line) in enumerate(m):
            for (j, x) in enumerate(line):
                if x == -1: return (i,j)
        return (-1,-1)

    def solve2(self, m, indetermines):
        self.compteur += 5
        i, j = first_unknown(m)
        m2 = [i[:] for i in m]
        m2[i][j] = 1
        m2, indetermines2 = solve(m2, indetermines)
        m3 = [i[:] for i in m]
        m3[i][j] = 0
        m3, indetermines3 = solve(m2, indetermines)




##########################
# Traitement de l'entrée :
##########################

nom = raw_input()
l = logimage()
l.fromtext()
m, indetermines = l.solve([[-1 for i in xrange(l.m)] for j in xrange(l.n)])


# Affichage :
lignes = l.lignes
colonnes = l.colonnes
n = l.n
if indetermines != 0:
    for i in m:
        out = " ".join([{0: "-", 1: "X", -1: "?"}[a] for a in i])
        print(out)
else:
    max_c = max([i[0] for i in colonnes])
    max_l = max([i[0] for i in lignes])
    txt = ""
    im = Image.new("RGBA",((max_l + l.m)*18,(max_c + n)*18),(255,255,255))
    draw = ImageDraw.Draw(im)

    for i in xrange(max_c):
        out = " " * max_l * 3
        txt += out
        for c in colonnes:
            if max_c - c[0] <= i :
                out = "{:<3}".format(c[i - max_c + c[0] + 1])
                txt += out
            else:
                out = "   "
                txt += out
        draw.text( (0,i * 18), unicode(txt,'UTF-8'), "Black")
        txt = ""
    for i in xrange(l.m+1):
        draw.line(((max_l+i)*18-5,0,(max_l+i)*18-5,(max_c+n)*18-5), "Black")
        if (i % 5 == 0):
            draw.line(((max_l+i)*18-4,0,(max_l+i)*18-4,(max_c+n)*18-5), "Black")
    for j,li in enumerate(lignes):
        out=("{"+":>{0}".format(max_l*3)+"}").format("".join(["{:<3}".format(
            i)for i in li[1:] if not i == 0]))
        draw.text( (0,(max_c+j) * 18), unicode(out,'UTF-8'), "Black")
        draw.line((0,(max_c+j)*18-5,(max_l+l.m)*18-5,(max_c+j)*18-5), "Black")
        if (j % 5 == 0):
            draw.line((0,(max_c+j)*18-4,(max_l+l.m)*18-5,(max_c+j)*18-4), "Black")
    draw.line((0,(max_c+n)*18-5,(max_l+l.m)*18-5,(max_c+n)*18-5), "Black")
    if (n % 5 == 0):
        draw.line((0,(max_c+n)*18-4,(max_l+l.m)*18-5,(max_c+n)*18-4), "Black")

    logo = Image.open(str(l.compteur/5+1)+".png")
    a,b=logo.size
    factor = min((max_l*18-6.)/a,(max_c*18-6.)/b)
    lo = logo.resize((int(a*factor),int(b*factor)),Image.NEAREST)
    a,b=lo.size
    im.paste(lo, (0,0,a,b),lo)
    if sys.argv[-1] == "1":
        folder = '../parus/'
    else:
        folder = '../'
    im.save(folder + 'logimage_'+nom+'.png', "PNG")
    for i,a in enumerate(m):
        for j,b in enumerate(a):
            if b == 1:
                draw.rectangle((((j+max_l)*18-5,(i+max_c)*18-5),
                ((j+max_l+1)*18-5,(i+max_c+1)*18-5)), "Black")
    del draw
    im.save(folder + 'logimage_'+nom+'_corrige.png', "PNG")
print(l.compteur / 5 + 1)
print(time.clock() - temps)
