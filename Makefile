DIR = ~/Téléchargements/
PWD = $(shell pwd)

all: $(subst .txt,.png,$(wildcard samples/*/*.txt))

%: logimage_%.png
	
logimage_%.txt: $(DIR)%.jpg
	./soft/img2txt.py $(DIR)$*.jpg $* > $@

%.png: %.txt
	cd $(dir $<) && $(PWD)/soft/haskell/main < $(notdir $<)

clean:
	rm -f samples/*/*.png
	rm -f *.png

.SECONDARY:
.PHONY: clean all
